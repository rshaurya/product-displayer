import React from 'react';
import { Card, CardBody,
    CardTitle, CardSubtitle } from 'reactstrap';

const shoppingCard = (props) => {
    return (
        <Card className="col-8 col-sm-4 col-md-3 m-3">
        <CardBody className="d-flex flex-column justify-content-between">
          <CardTitle>{props.title}</CardTitle>
          <CardSubtitle>{props.price}</CardSubtitle>
        </CardBody>
      </Card>
    );
}

export default shoppingCard;