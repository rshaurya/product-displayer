import React from "react";
import ShoppingCard from "../shoppingCard/ShoppingCard";
import Pagination from "../pagination/Pagination";
import Sortby from "../sortby/Sortby";
import Search from "../search/Search";
import { Spinner } from "reactstrap";

class Container extends React.Component {
  state = {
    productCount: 0,
    products: {},
    sortedProductIds: [],
    error: null,
    isLoaded: false,
    currentPage: 1,
    productsPerPage: 54,
    width: 0,
    showSpinner: true,
    searchedProductIds: [],
    searching: false,
    currentFilter: 'popularity'
  };

  setCurrentPage = currentPage => {
    console.log("selected page is", currentPage);
    this.setState({
      currentPage: currentPage
    });
  };

  componentDidMount() {
    const proxyurl = "https://cors-anywhere.herokuapp.com/";
    const url =
      "https://s3.ap-south-1.amazonaws.com/ss-local-files/products.json";
    fetch(proxyurl + url)
      .then(res => res.json())
      .then(
        result => {
          this.setState(
            {
              isLoaded: true,
              productCount: result.count,
              products: result.products
            },
            () => {
              this.setState(
                {
                  sortedProductIds: Object.keys(this.state.products).sort(
                    (a, b) => {
                      return (
                        this.state.products[b].popularity -
                        this.state.products[a].popularity
                      );
                    }
                  )
                },
                () => {
                  this.setState({
                    showSpinner: false
                  });
                }
              );
            }
          );
        },
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
    this.updateWindowDimensions();
    window.addEventListener("resize", this.updateWindowDimensions);
  }

  updateWindowDimensions = () => {
    this.setState({ width: window.innerWidth });
    if (window.innerWidth <= 450) {
      this.setState({ productsPerPage: 25 });
    }
    if (window.innerWidth > 450) {
      this.setState({ productsPerPage: 54 });
    }
  };

  sortLTH = () => {
    this.setState({
      currentFilter: 'lth' 
    })
    if (this.state.searching) {
      const tempProductIds = this.state.searchedProductIds;
      tempProductIds.sort((a, b) => {
        return this.state.products[a].price - this.state.products[b].price;
      });
      this.setState({
        searchedProductIds: tempProductIds
      });
    } else {
      this.setState({
        sortedProductIds: Object.keys(this.state.products).sort((a, b) => {
          return this.state.products[a].price - this.state.products[b].price;
        })
      });
    }
  };

  sortHTL = () => {
    this.setState({
      currentFilter: 'htl' 
    })
    if (this.state.searching) {
      const tempProductIds = this.state.searchedProductIds;
      tempProductIds.sort((a, b) => {
        return this.state.products[b].price - this.state.products[a].price;
      });
      this.setState({
        searchedProductIds: tempProductIds
      });
    } else {
      this.setState({
        sortedProductIds: Object.keys(this.state.products).sort((a, b) => {
          return this.state.products[b].price - this.state.products[a].price;
        })
      });
    }
  };

  sortPopularity = () => {
    this.setState({
      currentFilter: 'popularity' 
    })
    if (this.state.searching) {
      const tempProductIds = this.state.searchedProductIds;
      tempProductIds.sort((a, b) => {
        return (
          this.state.products[b].popularity - this.state.products[a].popularity
        );
      });
      this.setState({
        searchedProductIds: tempProductIds
      });
    } else {
      this.setState({
        sortedProductIds: Object.keys(this.state.products).sort((a, b) => {
          return (
            this.state.products[b].popularity -
            this.state.products[a].popularity
          );
        })
      });
    }
  };

  searchText = searchString => {
    if (searchString === "") {
      this.setState({
        searching: false
      });
      console.log("state ", this.state.searching);
    } else {
      let searchKeywords = searchString.split(" ");
      let searchResId = Object.keys(this.state.products).filter(item => {
        let tempRes = false;
        let i;
        for (i = 0; i < searchKeywords.length; i++) {
          tempRes =
            tempRes ||
            this.state.products[item].title
              .toLowerCase()
              .split(" ")
              .indexOf(searchKeywords[i].toLowerCase()) > -1;
          if (tempRes === true) break;
        }
        return tempRes;
      });
      // searchResId.sort((a, b) => {
      //   return (
      //     this.state.products[b].popularity - this.state.products[a].popularity
      //   );
      // });
      this.setState({
        searchedProductIds: searchResId,
        searching: true
      },
      () => {
        if(this.state.currentFilter === 'popularity')
        {
          this.sortPopularity();
        }
        else if (this.state.currentFilter === 'lth')
        {
          this.sortLTH();
        }
        else {
          this.sortHTL();
        }
      });
    }
  };

  render() {
    let spinner, pagination, results;
    if (this.state.showSpinner) {
      spinner = (
        <Spinner color="primary" style={{ width: "3rem", height: "3rem" }} />
      );
      pagination = null;
    } else {
      spinner = null;
      if(this.state.searching){
        pagination = (
            <Pagination
              total={this.state.searchedProductIds.length}
              pageChange={this.setCurrentPage}
              perPage={this.state.productsPerPage}
              width={this.state.width}
            />
          );
      }
      else{
        pagination = (
            <Pagination
              total={this.state.productCount}
              pageChange={this.setCurrentPage}
              perPage={this.state.productsPerPage}
              width={this.state.width}
            />
          );
      }
      
    }

    if (!this.state.searching) {
      results = this.state.sortedProductIds
        .slice(
          (this.state.currentPage - 1) * this.state.productsPerPage,
          this.state.currentPage * this.state.productsPerPage
        )
        .map(key => {
          return (
            <ShoppingCard
              key={key}
              id={key}
              title={this.state.products[key].title}
              price={this.state.products[key].price}
            />
          );
        });
    } else {
      if (this.state.searchedProductIds.length === 0) {
        results = <p>No products found</p>;
      } else {
        results = this.state.searchedProductIds
          .slice(
            (this.state.currentPage - 1) * this.state.productsPerPage,
            this.state.currentPage * this.state.productsPerPage
          )
          .map(key => {
            return (
              <ShoppingCard
                key={key}
                id={key}
                title={this.state.products[key].title}
                price={this.state.products[key].price}
              />
            );
          });
      }
    }

    return (
      <div className="container margin-wrapper">
        <Sortby
          lowToHigh={this.sortLTH}
          highToLow={this.sortHTL}
          popularity={this.sortPopularity}
        />
        <Search search={this.searchText} />
        <div className="row justify-content-center">
          {spinner}
          {results}
        </div>
        <div className="row mt-4 justify-content-center">{pagination}</div>
      </div>
    );
  }
}

export default Container;
