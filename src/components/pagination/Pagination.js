import React from 'react';
import PaginationComponent from "react-reactstrap-pagination";
import "bootstrap/dist/css/bootstrap.min.css";

const pagination = (props) => {
    let paginationNumber =5;

    if(props.width<500){
        console.log("less than 450");
        paginationNumber = 1;
    }
    return (
        <div>
            <PaginationComponent totalItems={props.total} pageSize={props.perPage} onSelect={props.pageChange} maxPaginationNumbers={paginationNumber} />
        </div>
    );
} 

export default pagination;