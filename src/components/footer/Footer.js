import React from 'react';
import classes from './Footer.module.css';

const footer = () => {
    return (  
        <div className={ classes.Footer }>
            Coding Problem
        </div>
    )
}

export default footer;