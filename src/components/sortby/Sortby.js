import React from 'react';
import { Label } from 'reactstrap';
import styles from './Sortby.module.css';

const sortby = (props) => {

    const displayFilter = () => {
        const btn = document.getElementById('sortByButton');
        btn.classList.toggle('active');
        if(btn.nextElementSibling.style.maxHeight)
            btn.nextElementSibling.style.maxHeight = null;
        else
            btn.nextElementSibling.style.maxHeight = btn.nextElementSibling.scrollHeight + "px";
    }

    const removeActiveClass = () => {
        const sortButtons = document.getElementsByClassName('sortButton');
        console.log("btns ", sortButtons);
        let i;
        for (i = 0; i < sortButtons.length; i++) {
            sortButtons[i].classList.remove("activeTag");
          }
        // sortButtons.forEach((sortButton)=>{
        //     sortButton.classList.remove("activeTag");
        // })
    }

    const popularity = () => {
        const selectedTag = document.getElementById('popularity');
        removeActiveClass();
        selectedTag.classList.add("activeTag");
        props.popularity();
    }

    const lowToHigh = () => {
        const selectedTag = document.getElementById('lth');
        removeActiveClass();
        selectedTag.classList.add("activeTag");
        props.lowToHigh();
    }

    const highToLow = () => {
        const selectedTag = document.getElementById('htl');
        removeActiveClass();
        selectedTag.classList.add("activeTag");
        props.highToLow();
    }
    
    


    return(
        <div style={{margin: "0px -15px"}}>
            <div className={styles.filterRow}  >
            <span style={{fontWeight: "bold"}}>Sort By</span>
            <div id="popularity" className="sortButton activeTag" onClick={popularity} >Popularity</div>
            <div id="lth" className="sortButton" onClick={lowToHigh}>Price (Low to High)</div>
            <div id="htl" className="sortButton" onClick={highToLow}>Price (High to Low)</div>
            </div>
             <button id='sortByButton' className={styles.filterRowMobile} onClick={displayFilter} >Sort By (3)
             </button> 
             <div className={[" mb-4", styles.filterContent].join(' ')}>
                <Label className="m-2 col-lg-3 col-12">
                    <input defaultChecked type="radio" name="sort"  onClick={props.popularity}/>Popularity
                </Label>
                <Label className="m-2 col-lg-3 col-12">
                    <input type="radio" name="sort"  onClick={props.lowToHigh}/>Price(Low To High)
                </Label>
                <Label className="m-2 col-lg-3 col-12">
                    <input type="radio" name="sort"  onClick={props.highToLow}/>Price(High To Low)
                </Label>
            </div> 
            {/* <div className={["d-flex justify-content-center mb-4", styles.filterRow].join(' ')}>
                <Button color="primary" className="m-2 col-2" onClick={props.highToLow}>Price(High To Low)</Button>
                <Button color="primary" className="m-2 col-2" onClick={props.lowToHigh}>Price(Low To High)</Button>
                <Button color="primary" className="m-2 col-2" onClick={props.popularity}>Popularity</Button>
            </div> */}
        </div>
    )
}

export default sortby;