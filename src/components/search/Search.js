import React from 'react';
import { Button } from 'reactstrap';

class Search extends React.Component {

    state = {
        searchString: null
    }

    displayResults = () => {
        this.props.search(this.state.searchString);
    }
    
    render(){
    return (
        <div>
            <input type="search" name="search" onChange={event => {
                this.setState(
                    {
                        searchString: event.target.value
                    }
                )
            }}>
            </input>
            <Button color="primary" onClick={this.displayResults}>Submit</Button>{' '}
        </div>  
    );
    }
}

export default Search;