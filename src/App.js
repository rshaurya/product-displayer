import React from 'react';
import Header from './components/header/Header';
import Footer from './components/footer/Footer';
import Container from './components/container/Container';
import './App.css';

function App() {
  return (
    <div className="App">
      <Header></Header>
      <Container></Container>
      <Footer></Footer>
    </div>
  );
}

export default App;
